extends Node


enum TEAM {
	NONE,
	SEED,
	ROOT,
	TOWER,
	ENEMY
}


static func step_towards_point(start : Vector2, dest : Vector2, step : float) -> Vector2:
	var diff : Vector2 = dest - start
	if diff.length_squared() <= pow(step, 2):
		return diff
	return Vector2(step, 0).rotated(diff.angle())

