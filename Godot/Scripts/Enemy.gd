extends Area2D

class_name Enemy

var destination := Vector2()
var speed : float = 20


func _physics_process(delta : float) -> void:
	var step  = speed * delta
	translate(Global.step_towards_point(global_position, destination, step))


func _on_Target_death():
	queue_free()
