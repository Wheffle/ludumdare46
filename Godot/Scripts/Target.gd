extends Area2D

class_name Target


signal death

export(Global.TEAM) var team : int = Global.TEAM.NONE
export var max_hp : int = 10

onready var hp : int = max_hp


func damage(amount : float) -> void:
	hp = clamp(hp - amount, 0, max_hp)
	if hp <= 0:
		destroy()


func destroy() -> void:
	emit_signal("death")
