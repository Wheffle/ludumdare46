extends Node2D

class_name Projectile


var destination : Vector2 setget _set_destination
var speed : float = 300


func _physics_process(delta : float) -> void:
	var step = speed * delta
	translate(Global.step_towards_point(global_position, destination, step))
	if global_position == destination:
		queue_free()


func _set_destination(new_destination : Vector2) -> void:
	destination = new_destination
	rotation = (destination - global_position).angle()


func set_texture(new_texture : Texture) -> void:
	$Sprite.texture = new_texture

