extends Area2D


export(Global.TEAM) var team : int = Global.TEAM.NONE

var target : Target = null


func set_target(new_target : Target) -> void:
	if target != null:
		target.disconnect("death", self, "_target_death")
	target = new_target
	target.connect("death", self, "_target_death")


func _target_death() -> void:
	target.disconnect("death", self, "_target_death")
	target = null


func _on_Timer_timeout():
	var areas = get_overlapping_areas()
	if target != null and !areas.has(target):
		target = null
	if target == null:
		var closest_target : Target = null
		var closest_distance_squared : float = -1
		for area in areas:
			if area is Target and area.team == team:
				var candidate : Target = area
				if closest_distance_squared == -1:
					closest_target = candidate
					continue
				var dist2 = global_position.distance_squared_to(candidate.global_position)
				if dist2 < closest_distance_squared:
					closest_target = candidate
		if closest_target != null:
			set_target(closest_target)

