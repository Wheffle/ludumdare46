extends Node2D


signal spawn(enemy)

export var enemy_spawned : PackedScene


func _on_Target_death():
	queue_free()


func _on_SpawnTimer_timeout():
	var enemy = enemy_spawned.instance()
	enemy.global_position = global_position
	emit_signal("spawn", enemy)
