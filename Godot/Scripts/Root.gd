extends Area2D

class_name Root


var prev : Node = null
var next : Node = null


func randomize_frame() -> void:
	$Sprite.frame = randi() % $Sprite.vframes


func get_end_position() -> Vector2:
	return $EndPoint.global_position
