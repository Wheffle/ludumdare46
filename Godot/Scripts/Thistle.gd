extends Area2D


signal spawn_projectile(projectile)

const ProjectileScene = preload("res://Scenes/Projectile.tscn")

onready var cooldown_timer = $CooldownTimer
onready var seeker = $Seeker
onready var projectile_spawn = $ProjectileSpawn
onready var animated_sprite = $AnimatedSprite

export var projectile_texture : Texture
export var projectile_damage : float = 1.0
export var cooldown_time : float = 1.0

var ready_to_fire : bool = true
var root : Root


func _physics_process(delta : float) -> void:
	if ready_to_fire and seeker.target != null:
		_fire_projectile()


func _fire_projectile() -> void:
	ready_to_fire = false
	cooldown_timer.start(cooldown_time)
	var projectile : Projectile = ProjectileScene.instance()
	projectile.set_texture(projectile_texture)
	projectile.global_position = projectile_spawn.global_position
	projectile.destination = seeker.target.global_position
	seeker.target.damage(projectile_damage)
	emit_signal("spawn_projectile", projectile)
	animated_sprite.play("attack")


func _on_CooldownTimer_timeout():
	ready_to_fire = true


func _on_AnimatedSprite_animation_finished():
	animated_sprite.play("default")


func _on_Target_death():
	queue_free()
