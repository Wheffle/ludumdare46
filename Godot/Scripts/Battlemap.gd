extends Node2D


const RootScene = preload("res://Scenes/Root.tscn")

onready var camera_centroid = $CameraCentroid
onready var camera = $CameraCentroid/Camera2D
onready var root_layer = $RootLayer
onready var ground_layer = $GroundLayer
onready var flying_layer = $FlyingLayer
onready var selection_area = $SelectionArea
onready var root_build_shadow = $RootBuildShadow
onready var tile_map = $TileMap

enum {
	STATE_NONE,
	STATE_BUILDING
}
var state : int = STATE_NONE

var camera_speed : float = 1000
var camera_min_zoom : float = 0.5
var camera_max_zoom : float = 1.5
var camera_zoom_step = 0.05


var selection : Root = null


func _process(delta : float) -> void:
	selection_area.global_position = get_global_mouse_position()
	_process_camera(delta)
	if state == STATE_BUILDING:
		_process_building_shadow()


func _process_camera(delta : float) -> void:
	var camera_translate := Vector2()
	if Input.is_action_pressed("ui_right"):
		camera_translate.x += 1
	if Input.is_action_pressed("ui_left"):
		camera_translate.x -= 1
	if Input.is_action_pressed("ui_down"):
		camera_translate.y += 1
	if Input.is_action_pressed("ui_up"):
		camera_translate.y -= 1
	camera_centroid.translate(camera_translate.normalized() * camera_speed * camera.zoom.x * delta)


func _process_building_shadow() -> void:
	var selection_position : Vector2 = selection.get_end_position()
	root_build_shadow.global_position = selection_position
	var dir : Vector2 = get_global_mouse_position() - selection_position
	root_build_shadow.rotation = dir.angle()
	


func _input(event : InputEvent) -> void:
	if event is InputEventMouseButton and event.is_pressed():
		match(event.button_index):
			BUTTON_WHEEL_UP:
				if camera.zoom.x > camera_min_zoom:
					camera.zoom.x -= camera_zoom_step
				if camera.zoom.y > camera_min_zoom:
					camera.zoom.y -= camera_zoom_step
			BUTTON_WHEEL_DOWN:
				if camera.zoom.x < camera_max_zoom:
					camera.zoom.x += camera_zoom_step
				if camera.zoom.y < camera_max_zoom:
					camera.zoom.y += camera_zoom_step
			BUTTON_LEFT:
				match(state):
					STATE_NONE:
						var areas = selection_area.get_overlapping_areas()
						for area in areas:
							if area is Root:
								selection = area
								state = STATE_BUILDING
								root_build_shadow.visible = true
								break
					STATE_BUILDING:
						var root : Root = RootScene.instance()
						root.global_position = root_build_shadow.global_position
						root.rotation = root_build_shadow.rotation
						root.prev = selection
						root.randomize_frame()
						root_layer.add_child(root)
						selection = root
			BUTTON_RIGHT:
				match(state):
					STATE_BUILDING:
						root_build_shadow.visible = false
						selection = null
						state = STATE_NONE


func _spawn_unit(unit) -> void:
	ground_layer.add_child(unit)


func _spawn_projectile(projectile : Projectile) -> void:
	flying_layer.add_child(projectile)



func _on_Thistle_spawn_projectile(projectile):
	_spawn_projectile(projectile)


func _on_FarmHouse_spawn(enemy):
	_spawn_unit(enemy)
